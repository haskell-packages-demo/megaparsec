# megaparsec

Monadic parser combinators. https://www.stackage.org/package/megaparsec

* [*Monadic Parser Combinators*
  ](https://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf)
  1996 Graham Hutton and Erik Meijer