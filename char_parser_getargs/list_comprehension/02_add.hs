module Main where

-- import Data.Text (Text)
import Data.Void
import System.Environment
import Text.Megaparsec
import Text.Megaparsec.Char

type Parser = Parsec Void String

symbol :: Parser Char
-- symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
symbol = choice
    [ char '!'
    , char '#'
    ]

spaces :: Parser [Char]
-- spaces :: ParsecT Void String Data.Functor.Identity.Identity [Char]
spaces = many (char ' ')

-- Does not work!
pLine :: Parser Char
pLine = [l | _ <- [spaces] , l <- [symbol]] !! 0
  -- do
  -- _ <- spaces
  -- l <- symbol
  -- return l
-- "spaces" is removed before to be applied.
-- Another layer (monad) is added, there is no more action to the base one.

readExpr :: String -> String
readExpr input = case parse pLine "lisp" input of
    Left err -> "No match: " ++ show err
    Right val -> "Found value"

main :: IO ()
main = do 
        (expr:_) <- getArgs
        parseTest pLine expr
        putStrLn (readExpr expr)

-- Adapted from: https://en.m.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours

