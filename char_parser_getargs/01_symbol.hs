module Main where

-- import Data.Text (Text)
import Data.Void
import System.Environment
import Text.Megaparsec
-- import Text.Megaparsec.Char

type Parser = Parsec Void String

symbol :: Parser Char
-- symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
symbol = choice
    [ satisfy (== '!')
    , satisfy (== '#')
    ]

readExpr :: String -> String
readExpr input = case parse symbol "lisp" input of
    Left err -> "No match: " ++ show err
    Right val -> "Found value"

main :: IO ()
main = do 
        (expr:_) <- getArgs
        parseTest (symbol) expr
        putStrLn (readExpr expr)

-- Adapted from: https://en.m.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours