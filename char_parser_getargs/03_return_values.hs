module Main where

import Control.Monad
-- import Data.Text (Text)
import Data.Void
import System.Environment
import Text.Megaparsec
import Text.Megaparsec.Char

type Parser = Parsec Void String

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

symbol :: Parser Char
-- symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
symbol = choice
    [ char '!'
    , char '#'
    ]

spaces :: Parser [Char]
-- spaces :: ParsecT Void String Data.Functor.Identity.Identity [Char]
spaces = many (char ' ')

parseString :: Parser LispVal
parseString = do
                char '"'
                x <- many (anySingleBut '"')
                char '"'
                return $ String x

parseAtom :: Parser LispVal
parseAtom = do 
              first <- letterChar <|> symbol
              rest <- many (letterChar <|> digitChar <|> symbol)
              let atom = first:rest
              return $ case atom of 
                         "#t" -> Bool True
                         "#f" -> Bool False
                         _    -> Atom atom

parseNumber :: Parser LispVal
parseNumber = liftM (Number . read) $ many digitChar

parseExpr :: Parser LispVal
parseExpr = parseAtom
         <|> parseString
         <|> parseNumber

readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
    Left err -> "No match: " ++ show err
    Right _ -> "Found value"


main :: IO ()
main = do 
        (expr:_) <- getArgs
        -- parseTest parseExpr expr
        putStrLn (readExpr expr)

-- Adapted from: https://en.m.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours

